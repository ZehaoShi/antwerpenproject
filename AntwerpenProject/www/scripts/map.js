﻿let routeMap;
let currentLocationMap = null;
let directionsDisplay = null;
let currentPosition = null;
let directionsService = new google.maps.DirectionsService();
let latitude = 0.0;
let longitude = 0.0;
let latitudeAndLongitude;
let poly = null;
let marker = new google.maps.Marker();
let id = null;
let mapZoom = 16;
let textOutputInterval = null;
let distanceInMeters = 0;
var totalWalkingDistance = 0;
var destination;
var text = document.getElementById("distanceDisplay");


function init() {
        
    directionsService = new google.maps.DirectionsService();
    // route planner
    directionsDisplay = new google.maps.DirectionsRenderer();


    let mapOptions = { zoom: 10, center: currentLocationMap };
    currentLocationMap = new google.maps.Map(document.getElementById('mapDiv'), mapOptions);
    directionsDisplay.setMap(currentLocationMap);

    poly = new google.maps.Polyline({
        strokeColor: '#f00',
        strokeOpacity: 0.5,
        strokeWeight: 7
    });
    poly.setMap(currentLocationMap);

    // add directions to the route
    directionsDisplay.setPanel(document.getElementById('directions'));
    let infoWindow = new google.maps.InfoWindow({
        content: "You are here!"
    })

    if (navigator.geolocation) {
        id = navigator.geolocation.watchPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            if (currentPosition == null) {
                marker == new google.maps.Marker({
                    position: { lat: pos.lat, lng: pos.lng },
                    map: currentLocationMap,
                    title: "You",
                    icon: {
                        url: "http://maps.google.com/mapfiles/ms/icons/pink-dot.png"
                    }
                });
            }
            
            path = poly.getPath();
            path.push(new google.maps.LatLng(pos.lat, pos.lng));
            totalWalkingDistance = totalWalkingDistance + google.maps.geometry.spherical.computeLength(path);

            text.innerHTML = Math.floor(totalWalkingDistance) + " m";

            currentPosition = pos;
            infoWindow.setContent('YOU ARE HERE');
            infoWindow.open(currentLocationMap, marker);
            marker.setPosition(new google.maps.LatLng(pos.lat, pos.lng));
            currentLocationMap.setCenter(pos);

            console.log("moved");

            if (destination != null) {
                calculateRoute({ lat: destination.lat, lng: destination.lng });
            }
        }, function () {
            handleLocationError(true, infoWindow, currentLocationMap.getCenter());
        }, { enableHighAccuracy: true, timeout: 5000, maximumAge: 0, desiredAccuracy: 10, frequency: 1000 });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, currentLocationMap.getCenter());
    }
}


function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(currentLocationMap);
}

let travelMode = "DRIVING";

function calculateRoute() {

    let start = new google.maps.LatLng(currentPosition.lat, currentPosition.lng);
    let end = document.getElementById('end').value;

    let request = {
        origin: start,
        destination: end,
        travelMode: google.maps.TravelMode[travelMode]
    };

    directionsService.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
            console.log("distance = " + response.routes[0].legs[0].distance.value);
            map.setCenter(new google.maps.LatLng(deviceLocation.lat, deviceLocation.lng));
        }
    });
}

/* 点击按钮，下拉菜单在 显示/隐藏 之间切换 */
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// 点击下拉菜单意外区域隐藏
window.onclick = function (event) {
    if (!event.target.matches('.dropbtn')) {

        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
}

